package com.sda.basics.streams;

import java.util.function.Predicate;

public class FilterAgeOsoba implements Predicate<Osoba> {
    @Override
    public boolean test(Osoba osoba) {
        return osoba.getWiek() > 18;
    }
}
