package com.sda.basics.streams;

import java.util.function.Consumer;

public class ConsumerOsoba implements Consumer<Osoba> {
    @Override
    public void accept(Osoba osoba) {
        System.out.println(osoba);
    }
}
