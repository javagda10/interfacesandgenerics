package com.sda.basics.streams.zadanie1;

public class Komputer {
    int identyfikatorProduktu;
    double częstotliwośćProcesora;
    boolean czyProcesorMaTurbo;
    double wielkośćPamięciRAM;
    double wielkośćDysku;
    double cena;
    int ilośćProcesorów;
    String nazwa; //model/nazwa/marka
    int pobieranaMoc; // moc w kW

    public Komputer(int identyfikatorProduktu, double częstotliwośćProcesora, boolean czyProcesorMaTurbo, double wielkośćPamięciRAM, double wielkośćDysku, double cena, int ilośćProcesorów, String nazwa, int pobieranaMoc) {
        this.identyfikatorProduktu = identyfikatorProduktu;
        this.częstotliwośćProcesora = częstotliwośćProcesora;
        this.czyProcesorMaTurbo = czyProcesorMaTurbo;
        this.wielkośćPamięciRAM = wielkośćPamięciRAM;
        this.wielkośćDysku = wielkośćDysku;
        this.cena = cena;
        this.ilośćProcesorów = ilośćProcesorów;
        this.nazwa = nazwa;
        this.pobieranaMoc = pobieranaMoc;
    }
}
