package com.sda.basics.collections.set;

import java.util.*;

public class MainSetExample {
    public static void main(String[] args) {
//        List<Integer> integers = new ArrayList<>();

//        Scanner scanner = new Scanner(System.in);
//        String linia = scanner.nextLine().toLowerCase();

//        String[] słowa = linia.split(" ");

        List<String> slowa = Arrays.asList("abc", "def", "gha", "abc", "ABC", "cba", "bca");
        Set<String> strings = new HashSet<>(slowa);
//        for (String slowo : słowa) {
//            strings.add(slowo);
//        }
        System.out.println(strings);

        for (String slowo : strings) {
            System.out.println(slowo);
        }

        if (slowa.size() == strings.size()) {
            System.out.println("Brak duplikatów");
        } else {
            System.out.println("Ilość słów usuniętych: " + (slowa.size() - strings.size()));
        }


    }
}
