package com.sda.basics.collections.list;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        int[] integerss = new int[3];

//        ArrayList<String> integers = new ArrayList<>();
//        System.out.println(integers.size());
//
//        integers.add("1");  /// 0 -> 0
//        integers.add("2");  /// 1 -----
//        integers.add("3");  /// 2 -> 1
//
//        integers.remove(1);
//        integers.remove("2");
        ArrayList<Long> integers = new ArrayList<>();
        System.out.println(integers.size());

        integers.add(1123L);  /// 0 -> 0
        integers.add(2123L);  /// 1 -----
        integers.add(3123L);  /// 2 -> 1

        integers.remove(1);
        integers.remove(2L);

        Long element = integers.get(1);
        System.out.println(integers);
    }
}
