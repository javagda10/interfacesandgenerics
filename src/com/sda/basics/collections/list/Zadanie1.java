package com.sda.basics.collections.list;

import java.util.ArrayList;
import java.util.List;

public class Zadanie1 {
    public static void main(String[] args) {
        List<String> stringList = new ArrayList<>();

        stringList.add("cos");
        stringList.add("cos");
        stringList.add("cos");
        stringList.add("cos");
        stringList.add("cosInnego");

        stringList.remove("cos");
        stringList.remove("cos");

    }
}
