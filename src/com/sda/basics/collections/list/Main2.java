package com.sda.basics.collections.list;

import java.util.ArrayList;
import java.util.Collections;

public class Main2 {
    public static void main(String[] args) {
        ArrayList<Integer> lista = new ArrayList<>();
        lista.add(1);
        lista.add(5);
        lista.add(3);
        lista.add(7);
        lista.add(2);

        System.out.println(lista);

        Collections.sort(lista);

        System.out.println(lista);
    }
}
