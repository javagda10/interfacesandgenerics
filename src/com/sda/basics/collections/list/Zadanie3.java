package com.sda.basics.collections.list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.*;

public class Zadanie3 {
    public static void main(String[] args) {
//        List<Integer> list = new ArrayList<>();
//        list.add(1);
//        list.add(2);
//        list.add(3);
//        list.add(4);
//        list.add(5);
//
        List<Integer> list = new ArrayList<>();
        list.addAll(Arrays.asList(1, 2, 3, 4, 5, 6));

        System.out.println(list);
        list.add(7);
        System.out.println(list);
    }
}
