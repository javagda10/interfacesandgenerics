package com.sda.basics.interfaces.zad1;

public class Son implements IFamilyMember{
    @Override
    public void introduce() {
        System.out.println("Who's asking?");
    }

    @Override
    public boolean isAdult() {
        return false;
    }
}
