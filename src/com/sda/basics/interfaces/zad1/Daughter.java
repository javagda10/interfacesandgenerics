package com.sda.basics.interfaces.zad1;

public class Daughter implements IFamilyMember{
    @Override
    public void introduce() {
        System.out.println("I am daughter :)");
    }

    @Override
    public boolean isAdult() {
        return false;
    }
}
