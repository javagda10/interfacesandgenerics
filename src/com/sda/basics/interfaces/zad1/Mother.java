package com.sda.basics.interfaces.zad1;

public class Mother implements IFamilyMember{
    @Override
    public void introduce() {
        System.out.println("I am mother");
    }

    @Override
    public boolean isAdult() {
        return true;
    }
}
