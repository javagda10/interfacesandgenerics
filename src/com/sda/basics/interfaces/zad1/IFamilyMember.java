package com.sda.basics.interfaces.zad1;

public interface IFamilyMember {
    void introduce();
    boolean isAdult();
}
