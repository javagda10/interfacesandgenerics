package com.sda.basics.interfaces.ex2;

public interface Dzwoni {
    static String nrAlarmowy = "112";
    
    void zadzwon(String nrTel);
    void zadzwonNaNrAlarmowy();
}
