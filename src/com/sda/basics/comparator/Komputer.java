package com.sda.basics.comparator;

public class Komputer {
    private String nazwa;
    private double moc;

    public Komputer(String nazwa, double moc) {
        this.nazwa = nazwa;
        this.moc = moc;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public double getMoc() {
        return moc;
    }

    public void setMoc(double moc) {
        this.moc = moc;
    }

    @Override
    public String toString() {
        return "Komputer{" +
                "nazwa='" + nazwa + '\'' +
                ", moc=" + moc +
                '}';
    }
}
