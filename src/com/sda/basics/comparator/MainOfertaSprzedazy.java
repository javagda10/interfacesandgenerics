package com.sda.basics.comparator;

import java.util.*;

public class MainOfertaSprzedazy {
    public static void main(String[] args) {
        OfertaSprzedazy ofertaSprzedazy1 = new OfertaSprzedazy("a", 1);
        OfertaSprzedazy ofertaSprzedazy2 = new OfertaSprzedazy("b", 11);
        OfertaSprzedazy ofertaSprzedazy3 = new OfertaSprzedazy("c", 15);
        OfertaSprzedazy ofertaSprzedazy4 = new OfertaSprzedazy("d", 12);
        OfertaSprzedazy ofertaSprzedazy5 = new OfertaSprzedazy("e", 31);
        OfertaSprzedazy ofertaSprzedazy6 = new OfertaSprzedazy("f", 14);
        OfertaSprzedazy ofertaSprzedazy7 = new OfertaSprzedazy("g", 15);

        List<OfertaSprzedazy> ofertaSprzedazies = new ArrayList<>();
        ofertaSprzedazies.addAll(Arrays.asList(
                ofertaSprzedazy1,
                ofertaSprzedazy2,
                ofertaSprzedazy3,
                ofertaSprzedazy4,
                ofertaSprzedazy5,
                ofertaSprzedazy6,
                ofertaSprzedazy7
        ));

        Collections.sort(ofertaSprzedazies, new ComparatorOfert(false));
        System.out.println(ofertaSprzedazies);
    }

    public static class ComparatorOfert implements Comparator<OfertaSprzedazy> {
        private boolean kierunekSortowania;

        public ComparatorOfert(boolean kierunekSortowania) {
            this.kierunekSortowania = kierunekSortowania;
        }

        @Override
        public int compare(OfertaSprzedazy o1, OfertaSprzedazy o2) {
//            if(kierunekSortowania) {
//                if (o1.getCena() > o2.getCena()) {
//                    return 1;
//                } else if (o1.getCena() < o2.getCena()) {
//                    return -1;
//                }
//            }else{
//                if (o1.getCena() > o2.getCena()) {
//                    return -1;
//                } else if (o1.getCena() < o2.getCena()) {
//                    return 1;
//                }
//            }
            if (o1.getCena() > o2.getCena()) {
                return -1 * (kierunekSortowania == true ? -1 : 1);
            } else if (o1.getCena() < o2.getCena()) {
                return 1 * (kierunekSortowania == true ? -1 : 1);
            }
            return 0;
        }
    }
}
