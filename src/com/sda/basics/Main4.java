package com.sda.basics;

import java.util.Scanner;

public class Main4 {
    public static void main(String[] args) {
        String wyrazenie = new Scanner(System.in).nextLine();
        for (int i = 0; i < wyrazenie.length()/2; i++) {
            char znakOdPoczatku = wyrazenie.charAt(i);
            char znakOdKonca = wyrazenie.charAt(wyrazenie.length()-1-i);
            System.out.println("poczatek: " + znakOdPoczatku + " koniec: " + znakOdKonca);
            if(znakOdKonca != znakOdPoczatku){
                System.out.println("Nie jest palindromem!");
                break;
            }

        }
    }
}
