package com.sda.basics.generics;

import com.sda.basics.interfaces.zad1.IFamilyMember;

public class Pudelko <T extends IFamilyMember>{
    private T zawartosc;

    public Pudelko(T zawartosc) {
        this.zawartosc = zawartosc;
    }

    public T getZawartosc() {
        return zawartosc;
    }

    public void setZawartosc(T zawartosc) {
        this.zawartosc = zawartosc;
    }

    @Override
    public String toString() {
        return "Pudelko{" +
                "zawartosc=" + zawartosc +
                '}';
    }
}
