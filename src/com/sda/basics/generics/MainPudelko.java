package com.sda.basics.generics;

import com.sda.basics.interfaces.zad1.Father;
import com.sda.basics.interfaces.zad1.IFamilyMember;

public class MainPudelko {
    public static void main(String[] args) {
        Father ojciec = new Father();
        Pudelko<Father> member = new Pudelko<>(ojciec);
//        Pudelko<Double> liczba = new Pudelko<>(2.0);

    }

    public static <T extends Number> double zsumujDwieLiczby(T pierwsza, T druga){
        return (pierwsza.doubleValue() + druga.doubleValue());
    }
}
