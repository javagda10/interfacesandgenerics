package com.sda.basics.generics;

import com.sda.basics.interfaces.zad1.IFamilyMember;

public class Main {
    public static void main(String[] args) {
//        Object opakowanie = 8;
//
//
//        int osemka = (int) opakowanie;

        Para<String> para1 = new Para<>(null, "costam");
        Para<String> para2 = new Para<>("cos", "costam");
        Para<String> para3 = new Para<>("aaa", "costam");
        Para<String> para4 = new Para<>(null, null);
        Para<String> para5 = new Para<>("null", null);

        Para[] pary = new Para[]{para1, para2, para3, para4, para5};

        znajdzNiepuste(pary);
    }

    public static Para[] znajdzNiepuste(Para[] pary) {
        int licznik = 0;
        for (Para para : pary) {
            if (para.getLewa() != null &&
                    para.getPrawa() != null) {
                //
                licznik++;
            }
        }
        Para[] tablicaWynikowa = new Para[licznik]; // licznik =3
        int indeksWstawiania = 0;
        for (int i = 0; i < pary.length; i++) {
            Para para = pary[i];

            if (para.getLewa() != null &&
                    para.getPrawa() != null) {
                //
                tablicaWynikowa[indeksWstawiania] = para;
                indeksWstawiania++;
            }
        }

        return tablicaWynikowa;
    }

    public static <T> void wypisz10Razy(T obiekt) {

    }
}
