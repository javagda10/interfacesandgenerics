package com.sda.basics.generics;

public class Para <T>{
    private T lewa;
    private T prawa;

    public Para(T lewa, T prawa) {
        this.lewa = lewa;
        this.prawa = prawa;
    }

    public T getLewa() {
        return lewa;
    }

    public void setLewa(T lewa) {
        this.lewa = lewa;
    }

    public T getPrawa() {
        return prawa;
    }

    public void setPrawa(T prawa) {
        this.prawa = prawa;
    }

}
