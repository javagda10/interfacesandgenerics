package com.sda.basics;

import java.util.Scanner;

public class Main3 {
    public static void main(String[] args) {

        String wyrazenie = new Scanner(System.in).nextLine();

        int otwartych = 0, zamknietych = 0;
        for (int i = 0; i < wyrazenie.length(); i++) {
            if (wyrazenie.charAt(i) == ')') {
                zamknietych++;
            }
            if (wyrazenie.charAt(i) == '(') {
                otwartych++;
            }
            if(zamknietych > otwartych){
                System.out.println("Domknieto nawias bez otwierania");
                break;
            }
        }
        if(zamknietych == otwartych){
            System.out.println("Poprawna ilość nawiasów!");
        }

    }
}
